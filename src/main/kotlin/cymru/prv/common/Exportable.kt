package cymru.prv.common

import org.json.JSONObject

/**
 * @author Preben Vangberg
 */
interface Exportable {

    fun exportToJson(): JSONObject

}