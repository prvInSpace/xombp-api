package cymru.prv.common

import org.json.JSONObject
import java.time.LocalDate
import java.util.*

/**
 * Represents a single variable that is able to change
 * through time.
 *
 * @author Preben Vangberg
 * @since 1.0.0
 */
public class DatedVariable<T>(internal val default: T) {

    constructor(obj: JSONObject,
                key: String,
                converter: (Any) -> T = { it as T },
                changesKey: String = "${key}s",
                default: T = converter(key)):
            this(if(obj.has(key)) converter(obj.get(key)) else default)
    {
        if(obj.has(changesKey)){
            val changesObj = obj.getJSONObject(changesKey)
            for(key in changesObj.keys())
                changes[LocalDate.parse(key)] = converter(changesObj.get(key))
        }
    }

    /**
     * The map of all of the dates and values
     */
    private val changes = TreeMap<LocalDate, T>()

    public fun contains(needle: T): Boolean {
        return default == needle || changes.values.contains(needle)
    }


    /**
     * Sets the value at the given date to the value
     * given. If prune is set to true it will prune
     * the tree afterwards
     */
    public fun setValue(date: LocalDate, value: T, prune: Boolean = false){
        changes[date] = value
        if(prune)
            prune()
    }


    /**
     * Prunes the tree i.e removes redundant
     * set-points
     */
    public fun prune() {
        val entries = changes.entries.toList()
        var last = default
        for((key,value) in entries) {
            if (last == value)
                changes.remove(key)
            else
                last = value
        }
    }

    /**
     * Fetches the value of the variable at the
     * given date.
     *
     * Default date is today
     */
    public fun getValue(date: LocalDate = LocalDate.now()): T {
        val key = changes.floorKey(date) ?: return default
        return changes.getValue(key)
    }


    /**
     * Returns the number of set-points / dates
     * stored in the map.
     */
    public fun size(): Int {
        return changes.size
    }

    public fun getValues(date: LocalDate = LocalDate.now(), after: LocalDate = LocalDate.MIN, includeDefault: Boolean = true): List<BoundedDatedVariable<T>> {

        var from: LocalDate? = null
        var value = default
        val list = mutableListOf<BoundedDatedVariable<T>>()
        var previous: BoundedDatedVariable<T>? = null
        for((change, new) in changes.entries){
            if(change > date)
                break
            if(new != value){
                if((from != null || includeDefault) && change > after){
                    val next = BoundedDatedVariable(from, change.minusDays(1), value, null, previous)
                    if(previous != null)
                        previous.next = next
                    previous = next
                    list.add(previous)
                }
                value = new
                from = change
            }
        }

        // Add final thingy
        if(includeDefault || from != null) {
            val last = BoundedDatedVariable(from, null, value, null, previous)
            if(previous != null)
                previous.next = last
            list.add(last)
        }

        return list
    }

    public fun getBounds(date: LocalDate): BoundedDatedVariable<T>{
        val from = changes.floorKey(date)
        val to = changes.higherKey(date)?.minusDays(1)
        return BoundedDatedVariable(from, to, changes.getValue(date))
    }

}