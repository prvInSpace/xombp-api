package cymru.prv.common

import java.time.LocalDate

/**
 * @author Preben Vangberg
 */
data class BoundedDatedVariable<T>(
    val from: LocalDate?,
    val to: LocalDate?,
    val value: T,
    var next: BoundedDatedVariable<T>? = null,
    var previous: BoundedDatedVariable<T>? = null
)
