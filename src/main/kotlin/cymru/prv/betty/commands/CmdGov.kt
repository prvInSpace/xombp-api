package cymru.prv.betty.commands

import cymru.prv.betty.Betty

object CmdGov: Command(
    "Print extensive amount of information about a specific ministry",
    "[date/name]",
    { params ->
        if(params.size == 0 || params.peek().matches("[0-9]{4}(-[0-9]{2}){2}".toRegex())) {
            val date = fetchDate(params)
            Betty.XOMBP.ministries.getValue(date)
                ?.buildEmbed(date, params)
                ?: createError("Unable to fetch government")
        }
        else {
            val name = params.joinToString(" ")
            val search = name.lowercase().replace("^percy$".toRegex(), "percy regime")
            val ministries = Betty.XOMBP.ministries.getValues(includeDefault = false)
                .filter { it.value != null }
                .filter { it.value!!.name.lowercase() == search }
            when(ministries.size){
                0 -> createError("Unable to find a ministry with the name of '$name'")
                else -> {
                   ministries[0].value!!.buildEmbed()
                }
            }
        }
    }
)

{
}