package cymru.prv.betty.commands

import cymru.prv.betty.Betty
import net.dv8tion.jda.api.EmbedBuilder
import java.awt.Color


object CmdUser: Command(
    "Prints information about a user",
    "<name>",

    // Function that handles the command call
    { params ->

        val name = params.joinToString(separator = " ")
        val lowered = name.lowercase()

        if (Betty.XOMBP.users.containsKey(lowered))
            Betty.XOMBP.users.getValue(lowered).buildEmbed()
        else {
            val closest = closestMatch(lowered, Betty.XOMBP.users)

            val builder = EmbedBuilder()
            builder.setColor(Color.red)
            builder.setTitle("Unknown User")
            builder.setDescription("Unable to find user '$name'. Make sure that you have typed the name correctly")
            builder.addField(
                "Did you mean",
                closest.map { it.name }.joinToString(separator = "\n"),
                false
            )
            builder.build()
        }
    }
)