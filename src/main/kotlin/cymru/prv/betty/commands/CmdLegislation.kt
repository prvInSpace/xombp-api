package cymru.prv.betty.commands

import cymru.prv.betty.Betty
import cymru.prv.xombp.legislation.Legislation
import net.dv8tion.jda.api.EmbedBuilder
import net.dv8tion.jda.api.entities.MessageEmbed
import java.util.*

object CmdLegislation: Command(
    listOf(
        "Prints information about a specific piece of legislation",
        "To fetch information about a specific bill write `leg <id>` (for example leg B050)",
        "To fetch information about a specific reading write `leg <id>[.passage] <C|L> <Reading> [vote id]`",
        "Passage defaults to 1, so if a bill only has one passage thish can be ignored.",
        "For the House, write either C or L to indicate whether you wnat the reading in the Commons or Lords.",
        "",
        "There are only a couple of reading types, these are as follows:",
        "First/1st/1",
        "Second/2nd/2",
        "Report/R",
        "Third/3rd/3",
        "Scehduled (When a motion was scheduled)",
        "Reading (A reading of a motion)",
        "Passed (When a motion was passed)",
        "",
        "Vote id is only required if there are several divisions in the given reading and you want to see how individual characters voted",
        "The id for divisions for amendments is the amendment name (for example 'A')",
        "For other divisions it is the same as the reading name, though this is only useful for votes during the 1st term."
    ).joinToString(separator = "\n"),
    "<id>\n<id>[.passage] [house reading [vote]]",
    ::leg
)

private val idFormat = "(QS|BUDGET|L?[BM][0-9]{3})\\.?[1-9]?".toRegex()
private fun leg(params: Queue<String>): MessageEmbed {
    return when {
        params.size == 0 -> createError("Not enough parameters provided", "Parameter Error")
        params.peek().uppercase().matches(idFormat)-> when (params.size) {
            else -> printLegislation(params)
        }
        else -> searchForBill(params)
    }
}

private fun searchForBill(params: Queue<String>): MessageEmbed{
    val name = params.joinToString(" ")
    val lowered = name.lowercase()
    val legs = Betty.XOMBP.legislation.values.filter {
        it.name.lowercase().contains(lowered)
    }

    return when(legs.size) {
        1 -> legs[0].buildEmbed()
        0 -> {
            val closest = closestMatch(lowered,
                Betty.XOMBP.legislation.values.associateBy { it.name.lowercase() }
            )
            getErrorMessage(
                name,
                closest,
                "Unable to locate legislation with the name $name. Are you sure you typed it in correctly?"
            )
        }
        else -> {
            getErrorMessage(
                name,
                legs,
                "Found several bills matching search pattern. Please confirm which one you meant."
            )
        }
    }
}

private fun getErrorMessage(name: String, closest: List<Legislation>, msg: String): MessageEmbed{
    val longest = closest.maxOf { it.id.length }
    val builder = EmbedBuilder()
    builder.setTitle("Unknown Legislation")
    builder.setDescription(msg)
    builder.setColor(Betty.defaultColor)
    builder.addField(
        "Did you mean",
        closest.map {
            "`%-${longest}s : ${it.name}`".format(it.id)
        }.sorted().joinToString("\n"),
        false
    )
    return builder.build()
}

/**
 * Prints information about a specific
 * piece of legislation
 */
private fun printLegislation(params: Queue<String>): MessageEmbed {
    val id = params.peek().uppercase().split(".")[0]
    return Betty.XOMBP.legislation[id]
        ?.buildEmbed(params=params)
        ?: createError("Could not locate bill/motion with the name '$id'", "Unknown Legislation")
}
