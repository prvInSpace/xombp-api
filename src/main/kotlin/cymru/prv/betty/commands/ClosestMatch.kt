package cymru.prv.betty.commands

import org.apache.commons.text.similarity.LevenshteinDistance

fun <T> closestMatch(needle: String, values: Map<String, T>, number: Int = 3): List<T> {

    val distances = mutableMapOf<T, Int>()
    values.keys.forEach {
        val value = values.getValue(it)
        val last = distances.getOrPut(value) { Int.MAX_VALUE }
        val new = LevenshteinDistance().apply(it, needle)
        if (new < last)
            distances[value] = new
    }
    return distances.entries
        .sortedBy { it.value }
        .take(number)
        .map { it.key }
}