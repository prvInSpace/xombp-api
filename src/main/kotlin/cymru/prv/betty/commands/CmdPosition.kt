package cymru.prv.betty.commands

import cymru.prv.betty.Betty

object CmdPosition: Command(
    "Prints information about a position",
    "[date] <name>",
    { params ->

        val date = fetchDate(params)

        if(params.isEmpty())
            createError(
                "Not enough parameters provided. You need to provide the name of the position you want information about",
                "Parameter Error")
        else {
            val name = params.joinToString(separator = " ")
            val lowered = name.lowercase()

            Betty.XOMBP.positions[lowered]
                ?.buildEmbed(date)
                ?: createError("Unknown position '$name'")
        }
    }
)
