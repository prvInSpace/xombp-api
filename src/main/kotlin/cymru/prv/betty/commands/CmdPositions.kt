package cymru.prv.betty.commands

import cymru.prv.betty.Betty
import net.dv8tion.jda.api.EmbedBuilder

object CmdPositions: Command(
    "Prints a list of positions",
    "<name>",
    {
        val builder = EmbedBuilder()

        builder.setTitle("Available Positions")
        builder.setColor(Betty.defaultColor)

        builder.setDescription(
            Betty.XOMBP.positions.values
                .distinct()
                .sortedBy { it.name.getValue() }
                .joinToString("\n") { "${it.name.getValue()} (${it.id})" }
        )

        builder.build()
    }
)
