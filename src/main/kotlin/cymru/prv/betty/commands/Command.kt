package cymru.prv.betty.commands

import net.dv8tion.jda.api.entities.MessageEmbed
import java.time.LocalDate
import java.util.*

/**
 * @author Preben Vangberg
 * @since 1.0.0
 */
open class Command (
    val desc: String,
    val params: String = "",
    val run: (Queue<String>) -> MessageEmbed,
    val list: Boolean = true
){

    companion object {
        fun fetchDate(params: Queue<String>): LocalDate {
            return when {
                params.isEmpty() -> LocalDate.now()
                params.peek().matches("[1-2][0-9][0-9][0-9]-[0-1][0-9]-[0-3][0-9]".toRegex()) -> {
                    LocalDate.parse(params.remove())
                }
                else -> LocalDate.now()
            }
        }
    }
}
