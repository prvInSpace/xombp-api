package cymru.prv.betty.commands

import cymru.prv.betty.Betty

/**
 * A command responsible for displaying
 * information about a specific party
 *
 * @author Preben Vangberg
 * @since 1.0.0
 */
object CmdParty: Command(
    "Prints information about a party",
    "[date] <name>",

    // What gets run if the command is called
    { params ->

        val date = fetchDate(params)
        val partyName = params.joinToString(separator = " ")

        if(partyName.isBlank())
            createError("You need to provide a party name", "Parameter Error")
        else
            Betty.XOMBP.parties[partyName.lowercase()]
                ?.buildEmbed(date)
                ?: createError("Unable to fetch party '$partyName'")
    }
)