package cymru.prv.api

import cymru.prv.betty.Betty
import cymru.prv.commons.json.JsonSerializable
import cymru.prv.commons.json.toJsonArray
import org.http4k.core.*
import org.http4k.routing.bind
import org.http4k.routing.path
import org.http4k.routing.routes
import org.json.JSONObject
import cymru.prv.xombp.House
import cymru.prv.betty.commands.fetchVoteString

class BettyAPI {

    val server: HttpHandler = routes(
        "/api/xombp/character" bind Method.GET to ::character,
        "/api/xombp/divisions/{id}" bind Method.GET to ::divisions,
        "/api/xombp/pages" bind Method.GET to ::pages,
        "/api/xombp/election/{id}" bind Method.GET to ::election,
        "/api/xombp/votes/{houseString:[cl]}/{id}" bind Method.GET to ::votes
    )

    private fun votes(request: Request): Response {
        val house = when(request.path("houseString")!!){
            "l" -> House.LORDS
            else -> House.COMMONS
        }
        return getResponseWithHeaders().body(
            JSONObject()
                .put("success", true)
                .put("result", fetchVoteString(house, request.path("id") ?: ""))
                .toString()
        )
    }

    private fun election(request: Request): Response {
        val id = request.path("id")
            ?: return failure("Missing election id")
        val election = Betty.XOMBP.elections
            .firstOrNull { it.name.lowercase() == id.lowercase() }
            ?: return failure("Missing election with name '$id'")
        return success(listOf(election))
    }

    private fun pages(request: Request): Response {
        return getResponseWithHeaders()
            .body(JSONObject(Betty.XOMBP.pages).toString())
    }

    private fun divisions(request: Request): Response {
        val id = request.path("id")
            ?: return failure("Missing legislation ID")
        val leg = Betty.XOMBP.legislation[id.uppercase()]
            ?: return failure("Legislation with id '$id' not found")
        return success(leg.getDivisions())
    }

    private fun character(request: Request): Response {
        val name = request.query("name")
            ?: return failure("Missing parameter 'name'")
        val char = Betty.XOMBP.characters[name.lowercase()]
            ?: return failure("Missing character '$name'")
        return success(listOf(char))
    }

    /**
     * Generates the basic JSON structure for a successful
     * API call. Success should be set to true and the
     * objects returned from the API should be in a field
     * called 'results'
     *
     * @param data The list of words from the API
     */
    private fun success(data: List<JsonSerializable>): Response {
        val resp = getResponseWithHeaders()
        val obj = JSONObject()
            .put("success", true)
            .put("results", data.toJsonArray())
        return resp.body(obj.toString())
    }


    /**
     * Creates the response for a failed API call.
     * Sends back a JSON object with the success flag
     * set to false and an error message.
     *
     * @param message The error message to return
     */
    private fun failure(message: String): Response {
        val resp = getResponseWithHeaders()
        val obj = JSONObject()
            .put("success", false)
            .put("message", message)
        return resp.body(obj.toString())
    }


    /**
     * Sets up the basic response with common headers
     * and stuff like that
     */
    private fun getResponseWithHeaders(): Response {
        return Response(Status.OK)
            .header("Content-Type", "application/json; charset=utf-8")
            .header("Access-Control-Allow-Origin", "*")
    }

}