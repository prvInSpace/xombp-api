package cymru.prv.xombp

/**
 * @author Preben Vangberg
 */
enum class Honours {
    GCTL,
    KCTL,
    DCTL,
    GCVO,
    KCVO,
    DCVO,
    KOBC,
    DOBC,
    OOBC,
    GBE,
    KBE,
    DBE,
    CBE,
    OBE,
    MBE,
    OR,
    MR,
    PC,
    MP
}