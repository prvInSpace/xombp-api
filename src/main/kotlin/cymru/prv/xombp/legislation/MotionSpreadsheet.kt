package cymru.prv.xombp.legislation

import cymru.prv.xombp.House
import cymru.prv.xombp.FullXOMBP
import java.net.URL
import java.time.LocalDate

/**
 * Represents a single Motion spreadsheet from the
 * data-set.
 *
 * @author Preben Vangberg
 * @since 1.0.0
 */
class MotionSpreadsheet(private val url: URL, val readings: IntArray = intArrayOf(5, 6, 7)) {

    /**
     * Goes through the data in the spreadsheet and adds
     * it to the XOMBP instance
     *
     * @param xombp XOMBP instance to add motions to
     */
    fun addMotions(xombp: FullXOMBP) {
        val lines = url.readText().lines().drop(2)
        var motion = Motion("", name="", status = LegislationStatus.UNKNOWN)
        lines.forEach {
            val values = it.split("\t")

            val status = when {
                values[readings[1]] == "Withdrawn" -> LegislationStatus.WITHDRAWN
                values[readings[2]] == "N/A" -> LegislationStatus.REJECTED
                else -> LegislationStatus.UNKNOWN
            }

            if (values[0].isNotBlank()) {
                motion = xombp.legislation.getOrPut(values[0]) {
                    val tempMotion = Motion(
                        id = values[0].trim(),
                        name = values[2].trim(),
                        status = status
                    )
                    xombp.legislation[tempMotion.id] = tempMotion
                    tempMotion
                } as Motion
            }

            val house = when {
                values[4].startsWith("C") -> House.COMMONS
                else -> House.LORDS
            }

            val dateRegex = "[0-9]{4}-[0-9]{2}-[0-9]{2}".toRegex()
            mapOf(
                ReadingType.SCHEDULED to values[readings[0]].trim(),
                ReadingType.READING to values[readings[1]].trim(),
                ReadingType.PASSED to values[readings[2]].trim()
            ).forEach {
                if(it.value.matches(dateRegex))
                    motion.addReading(house, it.key, LocalDate.parse(it.value))
            }
        }
    }
}