package cymru.prv.xombp.legislation

/**
 * List of different types of legislation
 *
 * @author Preben Vangberg
 * @since 1.0.0
 */
enum class LegislationType {
    MOTION, BILL, OTHER
}