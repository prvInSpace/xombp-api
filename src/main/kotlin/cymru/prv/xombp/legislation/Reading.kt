package cymru.prv.xombp.legislation

import cymru.prv.common.Exportable
import cymru.prv.commons.json.JsonSerializable
import cymru.prv.xombp.Character
import cymru.prv.xombp.Embedable
import cymru.prv.xombp.House
import net.dv8tion.jda.api.EmbedBuilder
import net.dv8tion.jda.api.entities.MessageEmbed
import org.json.JSONObject
import java.awt.Color
import java.time.LocalDate
import java.util.*

/**
 * Represents a single reading of a bill or motion
 *
 * @author Preben Vangberg
 * @since 1.0.0
 */
data class Reading(val passage: Passage, val house: House, val type: ReadingType, val date: LocalDate): JsonSerializable,
    Exportable {

    val votes = mutableMapOf<String, Map<Character, Vote>>()

    fun addVotesToEmbed(builder: EmbedBuilder, key: String? = null){

        if(date >= LocalDate.now() && votes.isEmpty()){
            builder.addField(
                "Scheduled reading",
                "This reading has yet to occur but it has been scheduled for $date.",
                false
            )
            return
        }

        if(votes.isEmpty()) {
            builder.addField(
                "Missing data",
                "No votes available for legislation ${passage.legislation.id} ${type.header} $date",
                false
            )
            return
        }

        val filter: (Map.Entry<String, Map<Character, Vote>>) -> Boolean = when {
            // If key is not provided just add all votes
            key == null -> { _ -> true }

            // For amendments
            key.matches("[A-Za-z]{1,2}".toRegex()) -> { it -> it.key.endsWith(key.uppercase())}

            // For bill readings
            else -> when(key) {
                "READING" -> { it -> it.key.matches("^[1-3].*".toRegex()) }
                "3" -> { it -> it.key == ReadingType.THIRD.header }
                "2" -> { it -> it.key == ReadingType.SECOND.header }
                else -> { _ -> true }
            }
        }

        val votes = this.votes.filter { filter(it) }

        when(votes.size){
            0 -> throw Exception("No vote with the name $key available for legislation ${passage.legislation.id} ${type.header} $date")
            1 -> addSingleDivision(builder, votes.keys.elementAt(0), votes.values.elementAt(0))
            else -> addMultipleDivisions(builder, votes)
        }
    }

    private fun addSingleDivision(builder: EmbedBuilder, header: String, vote:  Map<Character, Vote>){
        builder.setDescription(header)
        for (type in Vote.values()) {
            val votesOfType = vote.entries.filter {
                it.value == type
            }
            if (votesOfType.isNotEmpty()) {
                builder.addField(
                    "${type.title} (${votesOfType.size})",
                    votesOfType
                        .sortedWith(compareBy(
                            { it.key.affiliation.getValue(this.date).name.getValue(this.date) },
                            { it.key.name.getValue(this.date) }
                        )).joinToString(separator = "\n") {
                            val emote = it.key
                                .affiliation.getValue(this.date)
                                .emote.getValue(this.date)
                            "$emote ${it.key.getFullName(this.date)}"
                        },
                    false
                )
            }
        }
    }

    private fun addMultipleDivisions(builder: EmbedBuilder, votes:  Map<String, Map<Character, Vote>>){
        for (vote in votes) {

            val filteredVotes = Vote.values().map { type ->
                val votesOfType = vote.value.values.count { vote ->
                    type == vote
                }
                type to votesOfType
            }.filter { it.second != 0 }

            val longest = filteredVotes.map { it.first }.maxOf { it.title.length }

            builder.addField(
                vote.key,
                filteredVotes.map {
                    "%s `%-${longest}s : %d`".format(
                        it.first.emote,
                        it.first.title,
                        it.second
                    )
                }.joinToString(separator = "\n"),
                false
            )

        }
    }

    override fun toJson(): JSONObject {
        val obj = JSONObject()
        obj.put("house", house.toString())
        obj.put("date", date.toString())
        obj.put("type", type.toString())
        obj.put("typeHeader", type.header)

        obj.put("votes", JSONObject(
            votes.map {
                it.key to JSONObject(
                    it.value.map { vote -> vote.key.getFullName(date) to vote.value.name }.toMap()
                )
            }.toMap())
        )
        return obj
    }

    override fun exportToJson(): JSONObject {
        val obj = JSONObject()
        obj.put("date", date.toString())
        obj.put("stage", type.name)
        obj.put("house", house.name)
        obj.put("status", "FINISHED")

        val divisionObj = JSONObject()
        obj.put("divisions", divisionObj)
        var divisionID = 0
        votes.forEach {
            val division = JSONObject()
            divisionObj.put(divisionID.toString(), division)
            divisionID++
            division.put("title", it.key)
            division.put("votes", JSONObject(
                it.value.map { vote -> vote.key.id to vote.value.tps }.toMap()
            ))
        }

        return obj
    }


}


