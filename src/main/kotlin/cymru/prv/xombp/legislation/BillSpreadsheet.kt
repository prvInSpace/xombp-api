package cymru.prv.xombp.legislation

import cymru.prv.xombp.House
import cymru.prv.xombp.FullXOMBP
import java.net.URL

class BillSpreadsheet(
    private val url: URL,
    private val houseIndex: Int = 4,
    private val headerRows: Int = 2,
    private val house1Readings: IntArray = intArrayOf(5, 6, 7, 8),
    private val house2Readings: IntArray = intArrayOf(10, 11, 12, 13)
){

    /**
     * Adds the content of the file with the given
     * name to the XOMBP object.
     */
    fun addBills(xombp: FullXOMBP){
        val lines = url.readText().lines().drop(headerRows)
        var lastStatus = LegislationStatus.REJECTED
        var lastHouse1 = House.COMMONS
        var lastHouse2: House
        lines.forEach {
            val values = it.split("\t")
            val nameParts = values[0].trim().split(".")
            val bill = xombp.legislation.getOrPut(nameParts[0].uppercase()) {
                Bill(
                    id = values[0].trim(),
                    name= values[1].trim(),
                    status = when{
                        values[2].isBlank() -> lastStatus
                        else -> {
                            lastStatus = LegislationStatus.parse(values[2].trim())
                            lastStatus
                        }
                    }
                )
            } as Bill

            val passageNr = if (nameParts.size == 1) 1 else nameParts[1].toInt()
            val passage = Passage(bill)

            lastHouse1 = when {
                values[houseIndex].isBlank() -> lastHouse1
                values[houseIndex].startsWith("C") -> House.COMMONS
                else -> House.LORDS
            }
            lastHouse2 = when (lastHouse1) {
                House.COMMONS -> House.LORDS
                else -> House.COMMONS
            }

            bill.addPassage(passageNr, passage)
            addReadingsForHouse(values, passage, lastHouse1, house1Readings)
            addReadingsForHouse(values, passage, lastHouse2, house2Readings)
        }
    }


    /**
     * Goes through and searches for readings in the given
     * House within the given indexes of the row
     */
    private fun addReadingsForHouse(values: List<String>, passage: Passage, house: House, houseReadings: IntArray) {
        passage.addReading(values, houseReadings[0], house, ReadingType.FIRST)
        passage.addReading(values, houseReadings[1], house, ReadingType.SECOND)
        passage.addReading(values, houseReadings[2], house, ReadingType.REPORT)
        passage.addReading(values, houseReadings[3], house, ReadingType.THIRD)
    }

}



