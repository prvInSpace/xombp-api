package cymru.prv.xombp.legislation

import cymru.prv.xombp.House
import java.time.LocalDate

/**
 * Represent a single passage through the Houses of Parliament
 *
 * @author Preben Vangberg
 * @since 1.0.0
 */
class Passage(val legislation: Legislation) {

    /**
     * List of readings
     */
    internal val readings = mutableListOf<Reading>()

    /**
     * A regex to match a specific date
     */
    private val dateRegex = Regex("[0-9]+-[0-9]+-[0-9]+(/[0-9])?")


    /**
     * Adds the given reading to the passage if it is found
     * in the cell with the given index in values.
     */
    fun addReading(values: List<String>, index: Int, house: House, type: ReadingType) {
        if(values.size <= index)
            return
        if (values[index].matches(dateRegex)) {
            readings.add(
                Reading(
                    this,
                    house,
                    type,
                    LocalDate.parse(values[index].replace("/[0-9]$".toRegex(), ""))
                )
            )
        }
    }

}