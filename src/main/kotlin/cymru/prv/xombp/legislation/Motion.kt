package cymru.prv.xombp.legislation

import cymru.prv.betty.commands.createError
import cymru.prv.xombp.House
import net.dv8tion.jda.api.EmbedBuilder
import net.dv8tion.jda.api.entities.MessageEmbed
import org.json.JSONObject
import java.awt.Color
import java.time.LocalDate
import java.util.*

/**
 * Represents a motion. Only has one passage
 * however it can have several readings (useful for)
 */
class Motion(
    id: String,
    name: String,
    status: LegislationStatus
): Legislation(id, name, LegislationType.MOTION, status) {

    private val passage = Passage(this)

    fun addReading(house: House, type: ReadingType, date: LocalDate){
        if(!passage.readings.any { it.house == house && it.date == date && it.type == type })
            passage.readings.add(Reading(passage, house, type, date))
    }

    override fun getPassage(passageNumber: Int): Passage {
        return passage
    }

    override fun getDivisions(): List<Reading> {
        return passage.readings
            .filter { it.votes.isNotEmpty() }
    }

    private fun determineStatusBasedOnVotes(){
        val readings = passage.readings.filter { it.type == ReadingType.READING }
        val allReadingsSuccessful = readings.all { reading ->
            reading.votes.values.all { division ->
                division.count { it.value == Vote.AYE || it.value == Vote.CON } >
                        division.count { it.value == Vote.NO || it.value == Vote.NOT }
            }
        }


        when {
            readings.isEmpty() -> status = LegislationStatus.SCHEDULED
            readings.any { it.date > LocalDate.now() } -> status = LegislationStatus.READING
            allReadingsSuccessful -> status = LegislationStatus.PASSED
            !allReadingsSuccessful -> status = LegislationStatus.REJECTED
        }
    }

    override fun buildEmbed(date: LocalDate, params: Queue<String>): MessageEmbed {
        val builder = EmbedBuilder()

        builder.setTitle(getTitle())

        if(status == LegislationStatus.UNKNOWN)
            determineStatusBasedOnVotes()
        builder.addField(
            "Status",
            status.header,
            true
        )
        builder.addField(
            "House",
            passage.readings.map { it.house }.distinct().joinToString("\n") {
                "%s %s".format(it.emote, it.fullname())
            },
            true
        )
        builder.addBlankField(true)

        addDatesForReadingType(builder, "Scheduled", ReadingType.SCHEDULED)
        addDatesForReadingType(builder, "Reading", ReadingType.READING)
        addDatesForReadingType(builder, "Passed", ReadingType.READING)

        params.remove()
        val house = when{
            params.isEmpty() -> null
            params.peek().uppercase().startsWith("C") -> House.COMMONS
            params.peek().uppercase().startsWith("L") -> House.LORDS
            else -> return createError("Unknown house ${params.peek()}", "Parameter Error")
        }


        builder.setColor(
            when{
                status.color.isNotBlank() -> Color.decode(status.color)
                house != null -> house.color
                id.startsWith("L") -> House.LORDS.color
                else -> House.COMMONS.color
            }
        )

        // Print votes
        val divisions = passage.readings.filter { it.type == ReadingType.READING }
        if(divisions.size == 1){
            divisions[0].addVotesToEmbed(builder)
        }
        else if(house != null){
            val reading = passage.readings.find {
                it.house == house
            } ?: return createError("Unable to find reading for house $house")
            reading.addVotesToEmbed(builder)
        }

        return builder.build()
    }

    private fun addDatesForReadingType(builder: EmbedBuilder, header: String, type: ReadingType){
        val readings = passage.readings.filter { it.type == type}
        if(readings.isNotEmpty()){
            builder.addField(
                header,
                when(readings.size) {
                    1 -> readings[0].date.toString()
                    else -> readings.joinToString("\n"){
                        "${it.house.emote}`${it.date}`"
                    }
                },
                true
            )
        }
    }


    override fun exportToJson(): JSONObject {
        val obj = super.exportToJson()

        // Add stages and whatnot
        obj.put("stages", JSONObject(passage.readings
            .sortedBy { it.date }
            .mapIndexed { index, reading ->
            index.toString() to reading.exportToJson()
        }.toMap()))

        return obj
    }


}