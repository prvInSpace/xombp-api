package cymru.prv.xombp.legislation

/**
 * @author Preben Vangberg
 */
enum class Vote(val tps: String, val title: String, val emote: String) {
    AYE("IN_FAVOUR","The Ayes to the Right", "<:aye:556459322775699489>"),
    NO("AGAINST","The Noes to the Left", "<:no:556459344464576523>"),
    ABS("ABSTENTION","Abstentions", "<:abstain:556459330011004942>"),
    CON("IN_FAVOUR", "The Contents", "<:aye:556459322775699489>"),
    NOT("AGAINST", "The Not Contents", "<:no:556459344464576523>"),
    PRE("ABSTENTION", "The Presents", "<:abstain:556459330011004942>"),
    DNV("DID_NOT_VOTE","Did Not Vote", "<:independent:556471071902138368>")
}