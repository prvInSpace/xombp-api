package cymru.prv.xombp.legislation

/**
 * A set of enums representing the different
 * types of readings that might occur for bills
 * and motions.
 *
 * @author Preben Vangberg
 * @since 1.0.0
 */
enum class ReadingType(val header: String) {
    FIRST("1st Reading"),
    SECOND("2nd Reading"),
    REPORT("Report Stage"),
    THIRD("3rd Reading"),
    SCHEDULED("Scheduled"),
    READING("Reading"),
    PASSED("Passed")
}