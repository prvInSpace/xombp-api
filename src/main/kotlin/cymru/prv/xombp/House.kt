package cymru.prv.xombp

import java.awt.Color

/**
 * An enum containing the two different
 * Houses of Parliament
 *
 * @author Preben Vangberg
 * @since 1.0.0
 */
enum class House(val emote: String, val color: Color) {
    COMMONS("<:commons:858004961882800129>", Color.decode("#006547")),
    LORDS("<:lords:858004961962229772>", Color.decode("#b41d44"));

    fun fullname(): String {
        return "House of " +
                this.name
                    .lowercase()
                    .replaceFirstChar { it.uppercase() }
    }

}