package cymru.prv.xombp.election

import cymru.prv.xombp.Character

data class ElectionResults(
    val candidate: Character,
    val votes: Long,
    val seats: Long = -1
)