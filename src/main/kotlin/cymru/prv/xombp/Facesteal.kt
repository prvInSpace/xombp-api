package cymru.prv.xombp

import cymru.prv.commons.json.JsonSerializable
import org.json.JSONObject

/**
 * @author Preben Vangberg
 */

data class Facesteal(
    val desc: String,
    val link: String
): JsonSerializable {
    override fun toJson(): JSONObject {
        return JSONObject()
            .put("desc", desc)
            .put("link", link)
    }
}