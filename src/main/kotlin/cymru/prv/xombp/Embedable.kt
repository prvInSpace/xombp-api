package cymru.prv.xombp

import net.dv8tion.jda.api.entities.MessageEmbed
import java.time.LocalDate
import java.util.*

/**
 * @author Preben Vangberg
 */
interface Embedable {

    fun buildEmbed(date: LocalDate = LocalDate.now(), params: Queue<String> = LinkedList()): MessageEmbed

}
