package cymru.prv.xombp

import cymru.prv.common.BoundedDatedVariable
import cymru.prv.common.DatedVariable
import cymru.prv.commons.json.JsonSerializable
import cymru.prv.commons.json.toJsonArray
import net.dv8tion.jda.api.EmbedBuilder
import net.dv8tion.jda.api.entities.MessageEmbed
import org.json.JSONArray
import org.json.JSONObject
import java.time.LocalDate
import java.util.*

/**
 * @author Preben Vangberg
 * @since 1.0.0
 */
class Character(XOMBP: XOMBP, originalName: String, val user: User, obj: JSONObject = JSONObject()) : Embedable, JsonSerializable {

    val name = DatedVariable(obj, "name", default=originalName)
    private val aliases = mutableSetOf<String>()
    val honours: MutableMap<Honours, Honour> = mutableMapOf()
    val peerages: MutableList<Peerage> = mutableListOf()
    val affiliation = DatedVariable(XOMBP.parties.getValue("empty"))
    val positions = mutableSetOf<Position>()
    val isReverend = obj.optBoolean("isReverend", false)
    val activePrefix = DatedVariable<String?>(obj,"", default = null, changesKey = "activePrefix")

    var birth: LocalDate? = null
    var death: LocalDate? = null

    var facesteal: Facesteal = when {
        else -> {
            val facesteal = obj.optJSONObject("facesteal") ?: JSONObject()
            Facesteal(
                facesteal.optString("desc", ""),
                facesteal.optString(
                    "link",
                    "http://www.newdesignfile.com/postpic/2014/07/generic-user-icon-windows_352871.png"
                )
            )
        }
    }

    val id = NEXT_ID
    init {
        NEXT_ID++
    }

    companion object {
        var NEXT_ID = 100
        fun fetchCharacterName(name: String): String {
            var temp = name.split(",")[0]
            Honours.values().forEach { temp = temp.replace(it.toString(), "") }
            listOf(
                "The Most Honourable",
                "The Most Hon",
                "The Rt Hon",
                "The Rt Rev'd",
                "The Rt Revd",
                "Their Grace",
                "His Grace",
                "Reverend",
                "Revd.",
                "Rev.",
                "Sir",
                "Ser",
                "Dame",
                "FRS",
                "KCTL"
            ).forEach { temp = temp.replace(it, "") }
            return temp.trim()
        }

    }

    fun nameMatches(needle: String): Boolean {
        return name.contains(needle) || aliases.contains(needle)
    }

    init {
        user.characters.add(this)
        if (XOMBP.characters.containsKey(originalName.lowercase())) {
            val oldChar = XOMBP.characters.getValue(originalName.lowercase())
            println("${user.name}: Character $originalName already exist from user '${oldChar.user.name}'")
        }

        for(name in name.getValues().map { it.value }.distinct()) {
            XOMBP.characters[name.lowercase()] = this
        }

        if (obj.has("aliases")) {
            val strings = obj.getJSONArray("aliases")
            for (i in 0 until strings.length())
                aliases.add(strings.getString(i))
        }
        aliases.forEach { XOMBP.characters[it.lowercase()] = this }

        if (obj.has("affiliation")) {
            val affils = obj.getJSONObject("affiliation")
            for (key in affils.keys())
                affiliation.setValue(LocalDate.parse(key), XOMBP.parties.getValue(affils.getString(key)))
        }

        if (obj.has("peerages")) {
            val titles = obj.getJSONObject("peerages")
            for (key in titles.keys()) {
                val title = titles.getJSONObject(key)
                peerages.add(Peerage(LocalDate.parse(title.getString("granted")), key))
            }
        }

        if(obj.has("birth"))
            birth = LocalDate.parse(obj.getString("birth"))
        if(obj.has("death"))
            death = LocalDate.parse(obj.getString("death"))
    }

    fun getFullName(date: LocalDate = LocalDate.now()): String {

        val sb = StringBuilder()

        val activePeerages = peerages
            .filter { it.active.getValue(date) }

        if (activePeerages.isNotEmpty()) {
            val honourary = activePeerages
                .map { it.prefix }
                .distinct()
                .joinToString()
            if (honourary.isNotBlank())
                sb.append(honourary).append(" ")
        }

        if(isReverend)
            sb.append("Rev. ")

        val activeHonours = honours
            .filter { it.value.active.getValue(date) }
            .plus(user.honours.filter { it.value.active.getValue(date) })

        val suffixes = activeHonours.keys.sorted()
            .joinToString(separator = " ") { it.toString() }
        val prefixes = activeHonours.values.map { it.prefix }
            .filter { it.isNotEmpty() }
            .filter { !(it == "The Rt Hon" && activePeerages.isNotEmpty()) }
            .distinct()
            .joinToString(separator = " ")

        val prefixOverride = activePrefix.getValue(date)
        if(prefixOverride != null)
            sb.append(prefixOverride).append(" ")
        else if (prefixes.isNotBlank())
            sb.append(prefixes).append(" ")

        sb.append(name.getValue(date))

        if (suffixes.isNotBlank())
            sb.append(" ").append(suffixes)

        if (activePeerages.isNotEmpty()) {
            sb.append(", " + activePeerages.joinToString { it.title })
        }

        return sb.toString()
    }

    override fun buildEmbed(date: LocalDate, params: Queue<String>): MessageEmbed {
        val builder = EmbedBuilder()
        builder.setTitle(getFullName(date))

        builder.addField("User", user.name, true)

        if(birth != null)
            builder.addField("Born", birth.toString(), true)
        if(death != null)
            builder.addField("Died", death.toString(), true)
        if (facesteal.desc.isNotBlank())
            builder.addField("Facesteal", facesteal.desc, true)
        builder.setThumbnail(facesteal.link)

        val names = name.getValues(date)
        if(names.size > 1){
            builder.addField(
                "Former Names",
                names.joinToString("\n") {
                    "`%-10s - %-10s : %s`".format(
                        it.from ?: "Original",
                        it.to ?: "Current",
                        it.value
                    )
                },
                false
            )
        }

        if (peerages.isNotEmpty()) {
            val peerageDates = mutableListOf<BoundedDatedVariable<Peerage>>()
            peerages.forEach {
                it.active.prune()
                val values = it.active.getValues(date, includeDefault = false)
                    .filter { it.value }
                if(values.isEmpty())
                    return@forEach
                val from = values[0].from
                val to = values[values.size - 1].to
                peerageDates.add(BoundedDatedVariable(from, to, it))
            }
            if (peerageDates.isNotEmpty()) {
                builder.addField("Titles and Peerages",
                    peerageDates.sortedBy { it.from }.map {
                        val from = it.from?.toString() ?: ""
                        val to = it.to?.toString() ?: "Current"
                        val position = it.value.title
                        "`%-10s - %-10s : %s`".format(from, to, position)
                    }.joinToString(separator = "\n"),
                    false
                )
            }
        }

        val affiliations = affiliation.getValues(date, includeDefault = false)
        if (affiliations.isNotEmpty()) {
            val affil = affiliations.map {
                val from = it.from?.toString() ?: ""
                val to = it.to?.toString() ?: "Current"
                val party = it.value.name.getValue(it.to ?: date)
                builder.setColor(it.value.color.getValue(date))
                "`%-10s - %-10s : %s`".format(from, to, party)
            }.joinToString(separator = "\n")
            builder.addField(
                "Affiliation",
                affil,
                false
            )
        }

        if (positions.isNotEmpty()) {
            val dates = positions.flatMap { position ->
                position.holder.getValues(date)
                    .filter { it.value == this }
                    .map { Pair(position, it) }.toList()
            }.sortedBy { it.second.from }.toList()

            if (dates.isNotEmpty()) {
                builder.addField(
                    "Positions",
                    dates.map {
                        val from = it.second.from?.toString() ?: ""
                        val to = it.second.to?.toString() ?: "Incumbent"
                        val position = it.first.name.getValue(date)
                        "`%-10s - %-10s : %s`".format(from, to, position)
                    }.joinToString(separator = "\n"),
                    false
                )
            }
        }

        val activeHonours = honours.plus(user.honours)
            .filter { it.value.active.getValue(date) }

        if (activeHonours.isNotEmpty()) {
            builder.addField(
                "Honours",
                activeHonours.map {
                    "`%-10s : %s`".format(
                        it.value.active.getValues().get(1).from ?: LocalDate.MIN,
                        it.key
                    )
                }.joinToString(separator = "\n"),
                false
            )
        }

        builder.setFooter("Date: $date")
        return builder.build()
    }

    override fun toJson(): JSONObject {
        val obj = JSONObject()
        obj.put("type", "character")
        obj.put("name", name.getValue())
        obj.put("fullname", getFullName())
        obj.put("facesteal", facesteal.toJson())

        obj.put("affiliations", JSONArray(
            affiliation.getValues(includeDefault = false)
                .map { JSONObject()
                    .put("from", it.from)
                    .put("to", it.to)
                    .put("party", it.value.id)
                    .put("partyName", it.value.name.getValue(it.to ?: LocalDate.now()))
                }
        ))

        if(peerages.isNotEmpty()){
            obj.put("peerages", peerages.toJsonArray())
            obj.put("referredToAs", "The " + peerages.first().title.replace("^[0-9]+[a-z]+ ".toRegex(), ""))
        }

        obj.put("positions", JSONArray(positions
            .flatMap { position ->
                position.holder.getValues()
                    .filter { it.value == this }
                    .map {
                        JSONObject()
                            .put("id", position.id)
                            .put("title", position.name.getValue(it.to ?: LocalDate.now()))
                            .put("from", it.from)
                            .put("to", it.to)
                            .put("precededBy", it.previous?.value?.name?.getValue())
                            .put("succeededBy", it.next?.value?.name?.getValue())
                    }
            }
        ))

        return obj
    }


}