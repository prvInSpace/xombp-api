package cymru.prv.xombp

import cymru.prv.common.DatedVariable
import net.dv8tion.jda.api.EmbedBuilder
import net.dv8tion.jda.api.entities.MessageEmbed
import org.json.JSONObject
import java.time.LocalDate
import java.util.*

/**
 * @author Preben Vangberg
 */
class Position(val XOMBP: XOMBP, val id: String, obj: JSONObject): Embedable {

    val name = DatedVariable(obj.getString("name"))
    private val department = DatedVariable<String>(obj,"department")
    val holder = DatedVariable<Character?>(null)
    val active = DatedVariable(false)
    val type = PositionType.valueOf(obj.getString("type").uppercase())

    init {
        if(obj.has("names")){
            val names = obj.getJSONObject("names")
            for(key in names.keys())
                name.setValue(LocalDate.parse(key), names.getString(key))
        }

        name.getValues().forEach {
            XOMBP.positions[it.value.lowercase()] = this
        }

        val activeData = obj.getJSONObject("active")
        for(key in activeData.keys()){
            val date = LocalDate.parse(key)
            val new = activeData.getBoolean(key)
            if(!new)
                holder.setValue(date, null)
            active.setValue(date, new)
        }

        val privyCouncil = obj.optBoolean("privyCouncil", false)
        val holders = obj.getJSONObject("holder")
        for(dateString in holders.keys()){
            val date = LocalDate.parse(dateString)
            val charName = holders.getString(dateString)
            if(charName == "Vacant"){
                holder.setValue(date, null)
                continue
            }
            val char = XOMBP.characters.getValue(charName.lowercase())
            char.positions.add(this)
            holder.setValue(date, char)
            if(privyCouncil){
                char.honours.putIfAbsent(Honours.PC, Honour(date, "The Rt Hon"))
                char.honours.getValue(Honours.PC).active.setValue(date, true, prune = true)
            }
        }
    }

    override fun buildEmbed(date: LocalDate, params: Queue<String>): MessageEmbed {
        val builder = EmbedBuilder()
        builder.setTitle(name.getValue(date))
        builder.setDescription(department.getValue(date))


        val holders = holder.getValues(date, includeDefault = false)
        val incumbent = holders.last()
        if(incumbent.value != null){
            builder.setColor(incumbent.value.affiliation.getValue(date).color.getValue(date))
            if(incumbent.value.facesteal.link.isNotBlank())
                builder.setThumbnail(incumbent.value.facesteal.link)
            builder.addField("Incumbent", incumbent.value.getFullName(date), false)
            builder.addField("Since", incumbent.from.toString(), true)
        }


        val active = active.getValues(date, includeDefault = false)
        val founded = active[0].from
        if(founded != null)
            builder.addField("Established", founded.toString(), true)

        if(!active.last().value)
            builder.addField("Defunct", active.last().from.toString(), true)

        if(active.size > 1){
            active.joinToString("\n") {
                "%-10s - %-10s : %s".format(
                    it.from ?: "",
                    it.to ?: "Current",
                    it.value
                )
            }
        }

        val names = name.getValues()
        if(names.size > 1){
            builder.addField(
                "Former position title",
                names.dropLast(1).joinToString("\n") {
                    "`%s - %-10s : %s`".format(
                        it.from ?: founded,
                        it.to ?: "Currently",
                        it.value
                    )
                },
                false
            )
        }

        val departmentNames = department.getValues()
        if(departmentNames.size > 1){
            builder.addField(
            "Former department names",
                departmentNames.dropLast(1).joinToString("\n") {
                    "`%s - %-10s : %s`".format(
                        it.from ?: founded,
                        it.to ?: "Currently",
                        it.value
                    )
                },
                false
            )
        }

        if(holders.size > 1){
            builder.addField(
                "Former holders",
                holders.dropLast(1).joinToString("\n") {
                    "`%s - %-10s : %s`".format(
                        it.from ?: "",
                        it.to ?: "Incumbent",
                        it.value?.name?.getValue(it.to ?: date) ?: "Vacant"
                    )
                },
                false
            )
        }


        return builder.build()
    }

}