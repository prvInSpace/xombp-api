package cymru.prv.xombp

import cymru.prv.common.DatedVariable
import cymru.prv.commons.json.JsonSerializable
import org.json.JSONObject
import java.time.LocalDate

/**
 * @author Preben Vangberg
 */
class Peerage(val granted: LocalDate, val title: String): JsonSerializable {

    val prefix = when{
        title.contains("Dux") -> "Their Grace"
        title.contains("Duke") -> "His Grace"
        title.contains("Marquess") -> "The Most Honourable"
        title.contains("Bishop") -> "The Rt Revd"
        else -> "The Rt Hon"
    }
    val active = DatedVariable(false)
    init {
        active.setValue(granted, true)
    }

    override fun toJson(): JSONObject {
        return JSONObject()
            .put("granted", granted)
            .put("title", title)
            .put("prefix", prefix)
    }

}