package cymru.prv.xombp

import cymru.prv.betty.Betty
import cymru.prv.common.BoundedDatedVariable
import cymru.prv.common.DatedVariable
import net.dv8tion.jda.api.EmbedBuilder
import net.dv8tion.jda.api.entities.MessageEmbed
import org.json.JSONObject
import java.time.LocalDate
import java.util.*

class Ministry(val xombp: FullXOMBP, obj: JSONObject, val since: LocalDate): Embedable {

    val majorNumber = obj.getInt("majorNumber")
    val name = obj.getString("name")
    val nickname: String? = obj.optString("nickname", null)
    val parties = DatedVariable(obj.getJSONArray("parties")
        .map { xombp.parties.getValue(it as String) })

    init {

        if(obj.has("partyChanges")){
            val changes = obj.getJSONObject("partyChanges")
            for(key in changes.keys()){
                val date = LocalDate.parse(key)
                val list = changes.getJSONArray(key).map {
                    xombp.parties.getValue(it as String)
                }
                parties.setValue(date, list)
            }
        }
    }

    override fun buildEmbed(date: LocalDate, params: Queue<String>): MessageEmbed {

        val builder = EmbedBuilder()

        builder.setTitle("Her Majesty's Government")
        builder.addField(
            "Government",
            numberAsNumeral(),
            true
        )

        builder.addField(
            "Ministry",
            name,
            true
        )

        if(nickname != null){
            builder.addField(
                "Nickname",
                nickname,
                true
            )
        }
        else
            builder.addBlankField(true)

        val bounds = xombp.ministries.getBounds(since)
        builder.addField(
            "Since",
            since.toString(),
            true
        )
        builder.addField(
            "To",
            (bounds.to ?: "Incumbent").toString(),
            true
        )

        val pm = xombp.positions.getValue("pm").holder.getValues(after=since, date=bounds.to ?: LocalDate.now())
        builder.setColor(pm[0].value?.affiliation?.getValue(bounds.from!!)?.color?.getValue(date) ?: Betty.defaultColor)
        builder.addField(
            "Prime Minister" + when{
                pm.size > 1 -> "s"
                else -> ""
            },
            pm.joinToString("\n") {
                val from = it.from ?: since
                val to = it.to ?: LocalDate.now()
                if(from <= bounds.from && to >= (bounds.to ?: LocalDate.now()))
                    "%s %s".format(
                        it.value!!.affiliation.getValue(to).emote.getValue(to),
                        it.value.name.getValue(to)
                    )
                else
                    "%s `%s-%-10s` %s".format(
                        it.value!!.affiliation.getValue(to).emote.getValue(to),
                        it.from,
                        it.to ?: "Incumbent",
                        it.value.name.getValue(date)
                    )
            },
            false
        )

        addParties(builder, bounds)
        addMinisters(builder, bounds)
        return builder.build()
    }

    private fun addMinisters(builder: EmbedBuilder, bounds: BoundedDatedVariable<Ministry?>){
        val to = bounds.to ?: LocalDate.now()

        val positions = xombp.positions
            .filter { it.value.type == PositionType.GOVERNMENT }
            .filterNot { it.value.id == "pm" }
            .filter { it.value.active.getValues(after=bounds.from!!, date=to).any { it.value } }
            .toList()
            .distinctBy { it.second }
            .toMap()

        positions.values.forEach { position ->
            val ministers = position.holder
                .getValues(after=bounds.from!!, date=to)
                .sortedBy { it.from }
            if(ministers.size == 1 && ministers[0].value != null) {
                val minister = ministers[0].value!!
                builder.addField(
                    position.name.getValue(to),
                    "%s %s".format(minister.affiliation.getValue(to).emote.getValue(to), minister.name.getValue(to)),
                    false
                )
            }
            else if(ministers.size == 1){
                builder.addField(
                    position.name.getValue(to),
                    "Vacant",
                    false
                )
            }
            else if(ministers.size > 1) {
                builder.addField(
                    position.name.getValue(to),
                    ministers
                        .filter { position.active.getValue(it.to ?: LocalDate.now()) }
                        .joinToString("\n") {
                            val minister = it.value
                            if(minister != null) {
                                "`%s-%-10s`: %s %s".format(
                                    it.from,
                                    it.to ?: bounds.to ?: "Incumbent",
                                    minister.affiliation
                                        .getValue(it.to ?: LocalDate.now())
                                        .emote.getValue(it.to ?: LocalDate.now()),
                                    minister.name.getValue(to)
                                )
                            }
                            else {
                                "`%s-%-10s`: Vacant".format(
                                    it.from,
                                    it.to ?: bounds.to  ?: "Incumbent"
                                )
                            }
                    },
                    false
                )
            }
        }

    }

    private fun addParties(builder: EmbedBuilder, bounds: BoundedDatedVariable<Ministry?>){

        val periods = mutableListOf<BoundedDatedVariable<Party>>()
        val partiesStartDate = parties.getValue(since).associateWith {
            since
        }.toMutableMap()

        val changes = parties.getValues(after=since)

        changes.forEach {
            if(it.from == since)
                return@forEach
            val keysToRemove = mutableListOf<Party>()
            partiesStartDate.keys.forEach { party ->
                if(!it.value.contains(party)) {
                    periods.add(BoundedDatedVariable(partiesStartDate[party], it.from?.minusDays(1), party))
                    keysToRemove.add(party)
                }
            }
            keysToRemove.forEach { partiesStartDate.remove(it) }
            it.value.forEach { party ->
                if(!partiesStartDate.containsKey(party))
                    partiesStartDate[party] = it.from!!
            }
        }
        partiesStartDate.forEach { (party, localDate) ->
            periods.add(BoundedDatedVariable(localDate, bounds.to, party))
        }

        builder.addField(
            "Parties",
            periods
                .sortedWith(compareBy<BoundedDatedVariable<Party>> { it.from }.thenByDescending { it.to })
                    .joinToString("\n"){
                    val date = it.to ?: LocalDate.now()
                    if(it.from == since && it.to == bounds.to)
                        "%s %s".format(it.value.emote.getValue(date), it.value.name.getValue(date))
                    else
                        "%s `%s-%s` %s".format(
                            it.value.emote.getValue(it.from!!),
                            it.from,
                            it.to ?: "Present",
                            it.value.name.getValue(it.from)
                        )
                },
            false
        )
    }

    fun numberAsNumeral(): String {
        var string = "X".repeat(majorNumber / 10)
        string += "I".repeat(majorNumber % 10)

        string = string.replace("IIIII", "V")
        string = string.replace("VIIII", "IX")
        string = string.replace("IIII", "IV")

        return string
    }

}