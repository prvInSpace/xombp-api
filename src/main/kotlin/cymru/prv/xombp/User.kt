package cymru.prv.xombp

import cymru.prv.betty.Betty
import cymru.prv.common.DatedVariable
import net.dv8tion.jda.api.EmbedBuilder
import net.dv8tion.jda.api.entities.MessageEmbed
import org.json.JSONObject
import java.lang.Exception
import java.time.LocalDate
import java.util.*

/**
 * @author Preben Vangberg
 * @since 1.0.0
 */
class User(XOMBP: XOMBP, val name: String, obj: JSONObject = JSONObject()): Embedable {

    val characters = mutableSetOf<Character>()
    val honours: MutableMap<Honours, Honour> = mutableMapOf()
    val id = obj.optString("id", "")

    init{
        XOMBP.users[name.lowercase()] = this
        if(obj.has("aliases")){
            obj.getJSONArray("aliases").forEach {
                XOMBP.users[(it as String).lowercase()]
            }
        }
    }

    val activeCharacter = lazy {
        DatedVariable(obj,
            "",
            default = null,
            changesKey = "activeCharacter",
            converter = {
                if(it is String)
                    XOMBP.characters.getValue(it.lowercase())
                else
                    null
            }
        )
    }

    fun getActiveCharacter(date: LocalDate): Character? {
        return activeCharacter.value.getValue(date)
    }

    override fun buildEmbed(date: LocalDate, params: Queue<String>): MessageEmbed {
        val builder = EmbedBuilder()

        builder.setAuthor(name)

        if(id.isNotBlank()){
            try {
                val user = Betty.jda.retrieveUserById(id).complete()
                builder.setThumbnail(user.effectiveAvatarUrl)
                val guild = Betty.jda.getGuildById("556271746592800772")
                if(guild != null){
                    val member = guild.retrieveMember(user).complete()
                    if(member != null){
                        builder.setColor(member.color)
                        if(member.hasTimeJoined())
                            builder.addField("Joined", member.timeJoined.toLocalDate().toString(), true)
                        if(member.roles.any { it.name == "Speaker of the House" })
                            builder.addField("Speaker of the House", "True", true)
                        else if(member.roles.any { it.name == "Chair of Ways and Means" })
                            builder.addField("Chair of Ways and Means", "True", true)
                        else if(member.roles.any { it.name == "Deputy Speaker" })
                            builder.addField("Deputy Speaker", "True", true)
                        else if(member.roles.any { it.name == "Advisor to the Speakership" })
                            builder.addField("Advisory Council", "True", true)
                    }
                }
            }
            catch (e: Exception){
                e.printStackTrace()
            }
        }

        if(characters.isNotEmpty()){
            builder.addField(
                "Characters",
                characters.joinToString("\n") { "`${it.getFullName()}`" },
                false
            )
        }

        if(honours.isNotEmpty()){
            builder.addField(
                "Honours",
                honours.map { it.key to it.value.active.getValues(includeDefault = false)[0] }
                    .joinToString("\n"){
                        "`${it.second.from} : ${it.first}`"
                    },
                false
            )
        }

        return builder.build()
    }

}